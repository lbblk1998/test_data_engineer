#import something if needed
import pandas as pd
def import_raw_data(filepath, sep):
    # todo exercise 2
    df = pd.read_csv(filepath, compression='gzip', header=0, sep=sep, quotechar='"')
    return df


def process_data(df1, df2):
    result = pd.merge(df1, df2, left_on='identifiantproduit', right_on="pe_id")
    return result
    # todo exercise 2
    

def average_prices(df1,df2):
    # todo exercise 3
    merged_table = pd.concat([df1, df2])
    average = merged_table['prixproduit'].mean()  
    return average


def count_products_by_categories_by_day(df):
    df_grouped = df.groupby(['categorieenseigne']).size().reset_index(name='count')
    return df_grouped


def average_products_by_categories(df1, df2):
    df= pd.concat([df1, df2])
    df_grouped = df.groupby(['categorieenseigne']).size().reset_index(name='count')
    return df_grouped



if __name__ == '__main__':
    raw_data_20181017 = import_raw_data("data/17-10-2018.3880.gz", sep=';')
    raw_data_20181018 = import_raw_data("data/18-10-2018.3880.gz", sep=';')
    back_office = import_raw_data("data/back_office.csv.gz",sep=',')

    processed_data_20181017 = process_data(raw_data_20181017, back_office)
    #back_office est broken
    processed_data_20181018 = process_data(raw_data_20181018, back_office)
    print(processed_data_20181017)# It's empty because back_office is broken

    print(average_prices(raw_data_20181017, raw_data_20181018))
    print(count_products_by_categories_by_day(raw_data_20181017))
    print(average_products_by_categories(raw_data_20181017, raw_data_20181018))
    ...
