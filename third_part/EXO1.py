import gzip
import json

with gzip.open('data/products.json.gz', 'r') as f:
    product_list = json.loads(f.read().decode('utf-8'))
print(product_list[0])
def clean_cat(category):
    return ",".join([c for c in category.split(",") if c != "NULL"])

for product in product_list:
    product["idappcat"] = clean_cat(product["idappcat"])

print(product_list[0])