import pandas as pd
import os
from src import average_prices, count_products_by_categories_by_day, average_products_by_categories

def test_average_prices():
    df1 = pd.DataFrame({'nomproduit': ['Product A', 'Product B', 'Product C'],
                       'prixproduit': [10, 20, 30]})
    df2 = pd.DataFrame({'nomproduit': ['Product D', 'Product E', 'Product F'],
                       'prixproduit': [40, 50, 60]})
    expected_output = (10+20+30+40+50+60)/6
    #Call the function under test
    result = average_prices(df1,df2)
    # Ensure the output is as expected
    assert result == expected_output
    print("test_average_prices passed")

def test_count_products_by_categories_by_day():
    df = pd.DataFrame({'nomproduit': ['Product A', 'Product B', 'Product C','Product D','Product E','Product F'],
                       'categorieenseigne': ['Electronics', 'Electronics', 'Food','Food','Food','Clothing'],
                       'date': ['2022-01-01','2022-01-01','2022-01-01','2022-01-01','2022-01-02','2022-01-02']})
    expected_output = pd.DataFrame({'categorieenseigne': ['Clothing','Electronics','Food'], 'count': [1,2,3]})
    #Call the function under test
    result = count_products_by_categories_by_day(df)
    # Ensure the output is as expected
    pd.testing.assert_frame_equal(result, expected_output)
    print("test_count_products_by_categories_by_day passed")

def test_average_products_by_categories():
    df1 = pd.DataFrame({'nomproduit': ['Product A', 'Product B', 'Product C'],
                       'categorieenseigne': ['Electronics', 'Electronics', 'Food']})
    df2 = pd.DataFrame({'nomproduit': ['Product D', 'Product E', 'Product F'],
                       'categorieenseigne': ['Food', 'Food', 'Clothing']})
    expected_output = pd.DataFrame({'categorieenseigne': ['Clothing','Electronics','Food'], 'count': [1,2,3]})
    #Call the function under test
    result = average_products_by_categories(df1,df2)
    # Ensure the output is as expected
    pd.testing.assert_frame_equal(result, expected_output)
    print("test_average_products_by_categories passed")


test_average_prices()
test_count_products_by_categories_by_day()
test_average_products_by_categories()