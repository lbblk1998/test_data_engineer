from src import exercise_one,exercise_two, exercise_three

test_output = "1\n2\nThree\n4\nFive\nThree\n7\n8\nThree\nFive\n11\nThree\n13\n14\nThreeFive\n16\n17\nThree\n19\nFive\nThree\n22\n23\nThree\nFive\n26\nThree\n28\n29\nThreeFive\n31\n32\nThree\n34\nFive\nThree\n37\n38\nThree\nFive\n41\nThree\n43\n44\nThreeFive\n46\n47\nThree\n49\nFive\nThree\n52\n53\nThree\nFive\n56\nThree\n58\n59\nThreeFive\n61\n62\nThree\n64\nFive\nThree\n67\n68\nThree\nFive\n71\nThree\n73\n74\nThreeFive\n76\n77\nThree\n79\nFive\nThree\n82\n83\nThree\nFive\n86\nThree\n88\n89\nThreeFive\n91\n92\nThree\n94\nFive\nThree\n97\n98\nThree\nFive\n"


def test_exercise_one():
    import io
    import sys

    # Capture the output of the function
    captured_output = io.StringIO()
    sys.stdout = captured_output

    exercise_one()

    # Reset redirect.
    sys.stdout = sys.__stdout__

    # Get the output from the function
    output = captured_output.getvalue()

    # Check the output
    assert output == test_output
    
    print("ex1 test succeed")
def test_exercise_two():
    assert exercise_two([1]) == 2
    assert exercise_two([1, 2, 4, 6, 3, 7, 8]) == 5
    assert exercise_two([1, 2, 3, 5]) == 4
    assert exercise_two([2, 3, 4, 5, 6, 7, 8, 9, 10]) == 1
    assert exercise_two([]) == 1 # If the input array is empty, the missing number is 1
    print("All EXO2 tests pass!")


def test_exercise_three():
    assert exercise_three([1, -1]) == [1,-1]
    assert exercise_three([1, -2, 4, -6, 3, -7, 8]) == [1, -2, 3, -6, 4, -7, 8]
    assert exercise_three([-1, 2, -3, -5, 1]) == [-1, 1, -3, -5, 2]
    assert exercise_three([]) == []
    print("All EXO3 tests pass!")

test_exercise_one()
test_exercise_two()
test_exercise_three()