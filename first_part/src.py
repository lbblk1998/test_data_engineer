def exercise_one():
    for i in range(1, 101):
        if i % 3 == 0 and i % 5 == 0:
            print("ThreeFive")
        elif i % 3 == 0:
            print("Three")
        elif i % 5 == 0:
            print("Five")
        else:
            print(i)


def exercise_two(arr):
    # true length
    n = len(arr)+1
    # true sum
    total = (n * (n+1))/2
    sum_a = sum(arr)
    return total - sum_a

def exercise_three(arr):
    # Split the array into two parts: one for negative numbers and one for positive numbers
    negative_numbers = [x for x in arr if x < 0]
    positive_numbers = [x for x in arr if x >= 0]
    
    #True for p, False for n
    flag = [True if x >= 0 else False for x in arr]
    # Sort the positive numbers
    positive_numbers.sort()
    index = 0
    for i in range(0,len(arr)):
        if flag[i]:#positive
            arr[i] = positive_numbers[index]
            index += 1
    return arr



