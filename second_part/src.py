from sys import maxsize

class MySet(set):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
    def __add__(self, other):
        return MySet(super().__or__(other))
    
    def __sub__(self, other):
        return MySet(super().__sub__(other))


def decorator_check_max_int(func):
    # todo exercise 2
    return func


@decorator_check_max_int
def add(a, b):
    if(a+b)>maxsize:
        return maxsize
    return a + b


def ignore_exception(exception):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return None
        return wrapper
    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap



class MetaInherList(type):
    def __init__(cls, name, bases, dct):
        super(MetaInherList, cls).__init__(name, bases + (list,), dct)

class Ex:
    x = 4

class ForceToList(Ex, metaclass=MetaInherList):
    pass