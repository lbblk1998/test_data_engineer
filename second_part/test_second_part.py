from sys import maxsize

import pytest
import unittest
from src import MySet, div, raise_something, add, ForceToList, CacheDecorator


def test_my_set():
    a = MySet([1, 2, 3])
    b = MySet([1, 5, 100])
    assert a + b == {1, 2, 3, 5, 100}
    assert a - b == {2, 3}
    print("test_my_set succeed")


def test_max_int():
    assert add(5, 30) == 35
    assert add(maxsize, 1) == maxsize
    print("test_max_int succeed")



def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    print("test_ignore_exception succeed")
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)


def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4
    print("test_meta_list passed")

class TestCacheDecorator(unittest.TestCase):
    def test_cache_decorator(self):
        @CacheDecorator()
        def my_func(arg1, arg2):
            return arg1 * arg2

        # Test that the function is called and the result is cached
        result1 = my_func(2, 3)
        self.assertEqual(result1, 6)
        # Test that the cached result is returned
        result2 = my_func(2, 3)
        self.assertEqual(result1, result2)
        
    def test_cache_decorator_with_kwargs(self):
        @CacheDecorator()
        def my_func(arg1, arg2, kwarg1=1, kwarg2=2):
            return arg1 * arg2 + kwarg1 + kwarg2

        result1 = my_func(2, 3,kwarg1=4,kwarg2=5)
        self.assertEqual(result1, 15)
        # Test that the cached result is returned
        result2 = my_func(2, 3,kwarg1=4,kwarg2=5)
        self.assertEqual(result1, result2)
        
    def test_cache_decorator_with_same_args(self):
        @CacheDecorator()
        def my_func(arg1, arg2):
            return arg1 * arg2
        
        result1 = my_func(2, 3)
        self.assertEqual(result1, 6)
        # Test that different result is returned for different arg
        result2 = my_func(3, 3)
        self.assertNotEqual(result1, result2)

test_my_set()
test_max_int()
test_meta_list()
test_ignore_exception()

unittest.main()